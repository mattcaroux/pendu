import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;

public class ActionTemps implements EventHandler<ActionEvent>{


    private long tpsDepuisDebut, tpsPrec;
    private Label tps;

    public ActionTemps(Label tps) {
        this.tps = tps;
        this.tpsDepuisDebut = 0;
        this.tpsPrec = -1;
    }
    public void setTps(){
        long tpsEnSec = tpsDepuisDebut/1000;
        long heures = tpsEnSec/3600;
        long minutes = (tpsEnSec%3600)/60;
        long secondes = tpsEnSec%60;
        tps.setText(minutes + "m " + secondes + "s ");
    }




    @Override
    public void handle(ActionEvent event) {
        long tpsActuel = System.currentTimeMillis();
        if (tpsPrec != -1){
            long tpsEcoule = tpsActuel - tpsPrec;
            tpsDepuisDebut += tpsEcoule;
            setTps();
        }
        this.tpsPrec = tpsActuel;
    }
    
}
