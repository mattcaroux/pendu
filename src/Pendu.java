import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TitledPane;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData ;
import javafx.scene.control.ButtonType ;
import java.util.List;
import javafx.util.Duration;


import javax.sound.midi.ControllerEventListener;

import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;


/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * modèle du jeu
     **/
    private MotMystere modelePendu;
    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;
    /**
     * Liste qui contient les noms des niveaux
     */    
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;
    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;
    /**
     * la barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;
    /**
     * le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;
    /**
     * le text qui indique le niveau de difficulté
     */
    private Text leNiveau;
    /**
     * le chronomètre qui sera géré par une clasee à implémenter
     */
    private Timeline timeline;
    /**
     * le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;
    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;
    /**
     * le bouton Accueil / Maison
     */    
    private Button boutonMaison;
    /**
     * le bouton qui permet de (lancer ou relancer une partie
     */ 
    private Button bJouer;

    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */
    @Override
    public void init() {
        this.modelePendu = new MotMystere("./ressources/french.txt", 3, 10, MotMystere.FACILE, 10);
        this.lesImages = new ArrayList<Image>();
        this.chargerImages("./img");
        this.panelCentral = new BorderPane();
        this.motCrypte = new Text();
        this.pg = new ProgressBar();
        this.pg.setPadding(new Insets(10, 10, 10, 10));
        this.dessin = new ImageView(this.lesImages.get(0));
        // A terminer d'implementer
    }

    /**
     * @return  le graphe de scène de la vue à partir de methodes précédantes
     */
    private Scene laScene(){
        BorderPane fenetre = new BorderPane();
        fenetre.setTop(this.titre());
        fenetre.setCenter(this.panelCentral);
        return new Scene(fenetre, 800, 1000);
    }

    /**
     * @return le panel contenant le titre du jeu
     */
    private Pane titre(){
        BorderPane banniere = new BorderPane();


        Label titre = new Label("Jeu du Pendu");
        titre.setFont(new Font("Arial", 40));

        ImageView maison = new ImageView(new Image(new File("./img/home.png").toURI().toString()));
        maison.setFitWidth(36);
        maison.setPreserveRatio(true);


        ImageView parametre = new ImageView(new Image(new File("./img/parametres.png").toURI().toString()));
        parametre.setFitWidth(36);
        parametre.setPreserveRatio(true);

        ImageView info = new ImageView(new Image(new File("./img/info.png").toURI().toString()));
        info.setFitWidth(36);
        info.setPreserveRatio(true);


        HBox hb = new HBox();
        this.boutonMaison = new Button("", maison);
        this.boutonMaison.setOnAction(new RetourAccueil(modelePendu, this));
        this.boutonParametres = new Button("", parametre);
        Button boutonInformation = new Button("", info);
        boutonInformation.setOnAction(new ControleurRegles(modelePendu, this));
        hb.getChildren().addAll(this.boutonMaison, this.boutonParametres, boutonInformation);



        banniere.setLeft(titre);
        banniere.setRight(hb);
        banniere.setPadding(new Insets(20, 10, 20, 10));//top, right, bottom, left
        banniere.setStyle("-fx-background-color: #CCCCFF;");
        return banniere;
    }
    private Pane fenetreJeu(){
        //A implementer

        this.modelePendu.setMotATrouver();//permet de mettre à jour le mot à trouver en fonction du niveau

        BorderPane borderPane = new BorderPane();
        VBox vbox = new VBox();
        VBox pourLimage = new VBox();

        pourLimage.getChildren().add(this.dessin);
        pourLimage.setAlignment(Pos.CENTER);
        this.motCrypte.setText(this.modelePendu.getMotCrypte());
        this.motCrypte.setFont(new Font("Arial", 40));
        this.motCrypte.setTextAlignment(TextAlignment.CENTER);

        HBox clavbox = new HBox();
        this.clavier = new Clavier("ABCDEFGHIJKLMONPQRSTUVWXYZ", new ControleurLettres(modelePendu, this));
        clavbox.getChildren().addAll(this.clavier);
        clavbox.setSpacing(10);
        this.pg.setProgress(0);

        vbox.getChildren().addAll(this.motCrypte, pourLimage , this.pg, clavbox);
        vbox.setAlignment(Pos.CENTER);
        borderPane.setCenter(vbox);
        //le niveau
        VBox aDroite = new VBox();
        testNiveau();
        Label niveau = new Label("Niveau : " + this.leNiveau.getText());
        niveau.setFont(new Font("Arial", 20));
        niveau.setPadding(new Insets(10, 10, 10, 10));
        // le chronometre
        TitledPane tilted = new TitledPane();
        tilted.setCollapsible(false);
        tilted.setText("Chronomètre");
        Label tps = new Label("0h0m0s");
        tps.setFont(new Font(24));
        tilted.setContent(tps);
        ActionTemps action = new ActionTemps(tps);
        this.timeline = new Timeline(new KeyFrame(Duration.millis(100), action));
        this.timeline.setCycleCount(Animation.INDEFINITE);
        this.timeline.play();
        tilted.setPadding(new Insets(10, 10, 10, 10));

        Button nvMot = new Button("Nouveau mot");
        nvMot.setOnAction(new ControleurNvMot(modelePendu, this));
        nvMot.setPadding(new Insets(10, 10, 10, 10));
        aDroite.getChildren().addAll(niveau, tilted, nvMot);
        borderPane.setRight(aDroite);
        borderPane.setPadding(new Insets(10, 10, 10, 10));

        this.boutonMaison.setDisable(false);//permet de desactiver le bouton maison
        this.boutonParametres.setDisable(true);//permet de desactiver le bouton parametres

        return borderPane;
        
    }

     /**
     // * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de jeu
     // */
     private Pane fenetreAccueil() {

        this.boutonParametres.setDisable(false);//permet de réactiver le bouton parametres
        VBox vBox = new VBox();

        this.bJouer = new Button("Lancer une partie");
        this.bJouer.setOnAction(new ControleurLancerPartie(modelePendu, this));
        vBox.setPadding(new Insets(10, 20, 10, 10));//top right left botom
    
        TitledPane pane2 = new TitledPane();
        ToggleGroup tg = new ToggleGroup();
        RadioButton niveau = new RadioButton("Facile");
        RadioButton niveau2 = new RadioButton("Moyen");
        RadioButton niveau3 = new RadioButton("Difficile");
        RadioButton niveau4 = new RadioButton("Expert");


        niveau.setToggleGroup(tg);
        niveau2.setToggleGroup(tg);
        niveau3.setToggleGroup(tg);
        niveau4.setToggleGroup(tg);
        niveau.setSelected(true);



        niveau.setOnAction(new ControleurNiveau(modelePendu));
        niveau2.setOnAction(new ControleurNiveau(modelePendu));
        niveau3.setOnAction(new ControleurNiveau(modelePendu));
        niveau4.setOnAction(new ControleurNiveau(modelePendu));
        



        VBox vb = new VBox();
        vb.getChildren().addAll(niveau, niveau2, niveau3, niveau4);
        vb.setSpacing(10);
        vb.setPadding(new Insets(10, 10, 10, 10));
        pane2.setText("Niveau de difficulté");
        pane2.setContent(vb);
        pane2.setExpanded(true);
        pane2.setCollapsible(false);
        pane2.setPadding(new Insets(10, 10, 10, 10));
    
        vBox.getChildren().addAll(this.bJouer, pane2, vb);
        this.boutonMaison.setDisable(true);
        return vBox;
    }

    /**
     * charge les images à afficher en fonction des erreurs
     * @param repertoire répertoire où se trouvent les images
     */
    private void chargerImages(String repertoire){
        for (int i=0; i<this.modelePendu.getNbErreursMax()+1; i++){
            File file = new File(repertoire+"/pendu"+i+".png");
            System.out.println(file.toURI().toString());
            this.lesImages.add(new Image(file.toURI().toString()));
        }
    }

    public void modeAccueil(){
        // A implementer
        VBox vbox = new VBox();
        vbox.getChildren().add(this.fenetreAccueil());
        this.panelCentral.setCenter(vbox);
    }
    
    public void modeJeu(){
        // A implementer
        VBox vbox = new VBox();
        vbox.getChildren().add(this.fenetreJeu());
        this.panelCentral.setCenter(vbox);
        //Clavier clavier = new Clavier(je sais pas quoi mettre dans le constructeur pour l'instant);
    }
    
    public void modeParametres(){
        // A implémenter
    }
    public void testNiveau(){
        if(this.modelePendu.getNiveau() == 0){
            this.leNiveau = new Text("Facile");
        }
        if (this.modelePendu.getNiveau() == 1){
            this.leNiveau = new Text("Moyen");
        }
        if (this.modelePendu.getNiveau() == 2){
            this.leNiveau = new Text("Difficile");
        }
        if (this.modelePendu.getNiveau() == 3){
            this.leNiveau = new Text("Expert");
        }
    }

    /** lance une partie */
    public void lancePartie(){
        // A implementer
    }

    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage(){
        this.pg.setProgress((double)(this.modelePendu.getNbErreursMax() - this.modelePendu.getNbErreursRestants())/10);//permet de mettre a jour la progress bar
        System.out.println(this.modelePendu.getNbErreursMax() + " le nombre d'erreurs max");// permet de savoir le nombre d'erreur max
        System.out.println(this.modelePendu.getNbErreursRestants()+ " le nombre d'erreurs restantes");// permet de savoir le nombre d'erreur restantes
        System.out.println((double)(this.modelePendu.getNbErreursMax() - this.modelePendu.getNbErreursRestants())/10);// permet de savoir le resultat de la progression
        this.dessin.setImage(this.lesImages.get(this.modelePendu.getNbErreursMax()-this.modelePendu.getNbErreursRestants()));//permet de mettre a jour l'image
        this.motCrypte.setText(this.modelePendu.getMotCrypte());//permet de mettre a jour le mot crypté
        for(Button b : this.clavier.getClavier()){//permet de mettre a jour le clavier et desactiver les touches deja utilisées
            b.setDisable(this.modelePendu.getLettresEssayees().contains(b.getText()));
        }
        if (this.modelePendu.gagne()){
            this.popUpMessageGagne().showAndWait();
        }
        if(this.modelePendu.perdu()){
            this.popUpMessagePerdu().showAndWait();
        }
    }

    /**
     * accesseur du chronomètre (pour les controleur du jeu)
     * @return le chronomètre du jeu
     */
    public Chronometre getChrono(){
        // A implémenter
        return null; // A enlever
    }

    public Alert popUpPartieEnCours(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"La partie est en cours!\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
        
    public Alert popUpReglesDuJeu() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Le jeu du pendu est un jeu de mots où le joueur doit deviner un mot en proposant des lettres une par une.\n\n"
        + "Voici comment ça fonctionne :\n\n"
        + "1. Au début, un mot secret est sélectionné. Ce mot est affiché sous forme de tirets, un tiret pour chaque lettre du mot.\n\n"
        + "2. Le joueur propose une lettre en cliquant sur un bouton correspondant à cette lettre.\n\n"
        + "3. Si la lettre proposée est présente dans le mot secret, toutes les occurrences de cette lettre sont révélées. Sinon, le joueur accumule une erreur.\n\n"
        + "4. Pour chaque erreur, une partie du corps du pendu est dessinée progressivement. Le but du joueur est de deviner le mot avant que le dessin du pendu ne soit terminé.\n\n"
        + "5. Le joueur peut continuer à proposer des lettres tant qu'il n'a pas deviné le mot ou que le dessin du pendu n'est pas complet.\n\n"
        + "6. Le jeu se termine lorsque le joueur a deviné le mot correctement (victoire) ou lorsque le dessin du pendu est complet (défaite).\n\n"
        + "N'oubliez pas que le but du jeu est de deviner le mot avec le moins d'erreurs possible pour obtenir la victoire !\n\n"
        + "Amusez-vous bien en jouant au pendu !", ButtonType.OK);
        alert.setTitle("Règles du jeu du Pendu");
        alert.setHeaderText("Règles du jeu du Pendu");
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE); // Cela va me permettre d'ajuster la taille de la fenetre en fonction du texte
    
        return alert;
    }
    
    public Alert popUpMessageGagne(){
        //Mettre un bouton quitter ou rejouer
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Vous avez gagné ! Vous voulez recommencer", ButtonType.OK);        

        ButtonType rejouerBouttonType = new ButtonType("Rejouer", ButtonData.OK_DONE);
        ButtonType quitterBouttonType = new ButtonType("Quitter", ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(rejouerBouttonType, quitterBouttonType);

        Button rejouerButton = (Button) alert.getDialogPane().lookupButton(rejouerBouttonType);
        rejouerButton.setDefaultButton(true);
        rejouerButton.setPrefWidth(80);
        rejouerButton.setOnAction(new ControleurRejouer(this.modelePendu, this));

        Button quitterButton = (Button) alert.getDialogPane().lookupButton(quitterBouttonType);
        quitterButton.setDefaultButton(true);
        quitterButton.setPrefWidth(80);
        quitterButton.setOnAction(new RetourAcceuilDirect(this.modelePendu, this));

        return alert;
    }
    
    public Alert popUpMessagePerdu(){
        // A implementer    
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Vous avez perdu... Voulez vous réesayer", ButtonType.OK);

        ButtonType rejouerBouttonType = new ButtonType("Rejouer", ButtonData.OK_DONE);
        ButtonType quitterBouttonType = new ButtonType("Quitter", ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(rejouerBouttonType, quitterBouttonType);

        Button rejouerButton = (Button) alert.getDialogPane().lookupButton(rejouerBouttonType);
        rejouerButton.setDefaultButton(true);
        rejouerButton.setPrefWidth(80);
        rejouerButton.setOnAction(new ControleurRejouer(this.modelePendu, this));

        Button quitterButton = (Button) alert.getDialogPane().lookupButton(quitterBouttonType);
        quitterButton.setDefaultButton(true);
        quitterButton.setPrefWidth(80);
        quitterButton.setOnAction(new RetourAcceuilDirect(this.modelePendu, this));

        return alert;
    }
    /**
     * créer le graphe de scène et lance le jeu
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("IUTEAM'S - La plateforme de jeux de l'IUTO");
        stage.setScene(this.laScene());
        this.modeAccueil();
        stage.show();
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }    
}
